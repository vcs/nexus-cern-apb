FROM ansibleplaybookbundle/apb-base

RUN yum install -y jq python-pip rsync && \
    yum clean all && \
    pip install boto3 awscli

LABEL "com.redhat.apb.spec"=\
"dmVyc2lvbjogMS4wCm5hbWU6IG5leHVzLWNlcm4tYXBiCmRlc2NyaXB0aW9uOiBEZXBsb3lzIE5l\
eHVzMyBpbiBDRVJOIGVudmlyb25tZW50CmJpbmRhYmxlOiBGYWxzZQphc3luYzogb3B0aW9uYWwK\
bWV0YWRhdGE6CiAgZGlzcGxheU5hbWU6IG5leHVzLWNlcm4KICBkZXBlbmRlbmNpZXM6IFtdCnBs\
YW5zOgogIC0gbmFtZTogZGVmYXVsdAogICAgZGVzY3JpcHRpb246IFRoaXMgZGVmYXVsdCBwbGFu\
IGRlcGxveXMgTmV4dXMgT1NTIG9uIFMzIHN0b3JhZ2UKICAgIGZyZWU6IFRydWUKICAgIG1ldGFk\
YXRhOiB7fQogICAgcGFyYW1ldGVyczoKCiAgICAtIG5hbWU6IGFwYl9zM19hY2Nlc3Nfa2V5CiAg\
ICAgIHRpdGxlOiBTMyBBY2Nlc3MgS2V5CiAgICAgIGRlc2NyaXB0aW9uOiBUaGUgUzMgQWNjZXNz\
IEtleSB0byB1c2UgZm9yIHRoZSBzdG9yYWdlIG9mIHRoaXMgTmV4dXMgaW5zdGFuY2UKICAgICAg\
cmVxdWlyZWQ6IHRydWUKICAgICAgdHlwZTogc3RyaW5nCgogICAgLSBuYW1lOiBhcGJfczNfc2Vj\
cmV0X2tleQogICAgICB0aXRsZTogUzMgU2VjcmV0IEtleQogICAgICBkZXNjcmlwdGlvbjogVGhl\
IFMzIFNlY3JldCBLZXkgdG8gdXNlIGZvciB0aGUgc3RvcmFnZSBvZiB0aGlzIE5leHVzIGluc3Rh\
bmNlCiAgICAgIHJlcXVpcmVkOiB0cnVlCiAgICAgIHR5cGU6IHN0cmluZwoKICAgIC0gbmFtZTog\
YXBiX25leHVzX2FkbWluX3Bhc3N3b3JkCiAgICAgIHRpdGxlOiBOZXh1cyBBZG1pbiBQYXNzd29y\
ZAogICAgICBkZXNjcmlwdGlvbjogQWRtaW4gdXNlciBwYXNzd29yZCBmb3IgTmV4dXMgKGdlbmVy\
YXRlZCBpZiBlbXB0eSkKICAgICAgcmVxdWlyZWQ6IGZhbHNlCiAgICAgIHR5cGU6IHN0cmluZwog\
ICAgICBkaXNwbGF5X3R5cGU6IHBhc3N3b3JkCgojIFRPRE86IGFkbWluIGUtZ3JvdXAKIyBUT0RP\
OiBlbnRlcnByaXNlIHBsYW4="

COPY playbooks /opt/apb/project
COPY roles /opt/ansible/roles
# merge customizations into upstream nexus3-oss role
RUN cp -R /opt/ansible/roles/nexus3-oss-customizations/tasks /opt/ansible/roles/nexus3-oss && \
    chmod -R g=u /opt/{ansible,apb} && \
    # we need the following to create directories in /var/nexus to hold current groovy scripts
    mkdir /var/nexus/ && chmod 777 /var/nexus
USER apb
