# **Deprecated - as of 2022, please use https://gitlab.cern.ch/vcs/nexus-cern-operator instead**



# nexus-cern-apb

An application template for deploying [Nexus3 OSS](https://www.sonatype.com/nexus-repository-oss) at CERN

## How to deploy an instance of Nexus

Nexus deployments use OpenShift Persistent Volumes for artifact storage to provision an instance.
Nexus will be using it to store its data.

1. Create (if you don't have one already) an [e-group](https://cern.ch/e-groups) whose members will be granted admin
  privileges on the new instance. *Make sure that the creator of the instance is member of that group!* Note that
  only direct members of the group will be taken into account (nested groups are NOT currently supported).
1. [Create a web site from the Web Services portal](https://cern.ch/webservices-help/Websitemanagement/ManagingWebsitesatCERN/Pages/WebsitecreationandmanagementatCERN.aspx)
  using type "PaaS Web Application" (OpenShift).
1. Once you get the confirmation that the web site has been provisioned, navigate to your site details in
  the [Web Services portal](https://cern.ch/web) and follow the link to "Manage your site" to open the OpenShift Console.
1. Select the `nexus-cern` item in the Catalog and fill in the value for admin e-groups prepared in the
  previous steps (use the short name of the e-group, removing the domain name `@cern.ch`) and the size of Nexus storage.
  There must be [sufficient storage quota](https://cern.service-now.com/service-portal?id=kb_article&n=KB0004427)
  in your OpenShift project quota. Provisioning takes a few minutes.
1. The Nexus instance will be accessible at `https://<sitename>.web.cern.ch` and `https://cern.ch/<sitename>`

The configured e-group has full admin permissions on the provisioned Nexus instance. The default configuration is explained below.
See [Nexus3 help pages](https://help.sonatype.com/repomanager3/configuration) to configure Nexus and create artifact repositories.

## Service Level for Nexus data

Nexus artifact data and database are stored in [OpenShift Persistent Volumes](https://cern.service-now.com/service-portal?id=kb_article&n=KB0004361).
OpenShift Persistent Volumes provide a backup solution, thus **an instance can be fully restored from the contents its backup**.

### Protection against accidental data deletion or modification

OpenShift Persistent Volume includes a backup policy, this means that, even if the Nexus instance gets deleted,
the information will persist for 30 more days.
See [backup policy](https://cern.service-now.com/service-portal?id=kb_article&n=KB0004361).

### Security updates

Security vulnerabilities are announced in https://support.sonatype.com/hc/en-us/sections/203012668-Security-Advisories
(follow the article for notifications).

Security updates will be deployed centrally and restart all existing instances, with a short downtime (typically a few minutes).
Deployment of security updates will be announced on the [IT SSB](https://cern.ch/itssb).

## Initial configuration

### Authentication

A SSO proxy is in charge of performing user authentication, but takes no authorization decision. The following
authentication methods are implemented:

* for the Nexus web interface and API: SSO authentication with Shibboleth. No anonymous access is allowed. For API
  access in scripts, use [cern-get-sso-cookie](http://linux.web.cern.ch/linux/docs/cernssocookie.shtml)
* for repository access:
  * in general: LDAP authentication (i.e. user/password of a CERN account/service account)
  * certain repository types use token authentication instead
  * anonymous access if explicitly enabled in Nexus configuration (disabled by default)
  * basic authentication if you want to separate login from LDAP

NB: due to the design of the Nexus web interface, it is not possible to re-authenticate automatically with SSO when the Nexus session
expires. This means that once the user session expires, Nexus will complain that the user is not authenticated anymore and
**users need to refresh the page (typically pressing F5 or the reload button) in their web browser to re-authenticate**.
Nexus sessions are configured to expire after 4h by default but this can [be adjusted as necessary](https://groups.google.com/a/glists.sonatype.com/forum/#!topic/nexus-users/Spv3SVtO4Bs).

#### Basic authentication configuration steps

You have to login to Nexus with a user who has admin rights. Access the Nexus configuration settings by clicking the cog icon (top left corner between the logo and the component search field). In the left administration panel locate the "Users" element it is inside the Security section. Once the form loads click on the "create local user button". Fill out the required form and click on the "create local user" button.

### Authorization

Authorization decisions take place in Nexus via [roles](https://help.sonatype.com/repomanager3/security/roles).
E-groups can be used by [Mapping External Groups to Nexus Roles](https://help.sonatype.com/repomanager3/security/roles#Roles-MappingExternalGroupstoNexusRoles).

E-groups are available under the preconfigured "CERN xLDAP" external group provider.

**Important note:** only direct e-group members are taken into account. Members of nested e-groups (groups included in a group)
are currently ignored.

### Blob storage

During instance provisioning, an OpenShift Persistent Volumes `nexus-data` is created with the desired storage
quota specified when lunching the instance.

Then a file [Blob Store](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-BlobStores)
is configured automatically to use that persistent volume.

### Nexus database

Nexus uses an OrientDB database to store configuration.
This database is backed up and stored in [OpenShift Persistent Volumes](https://cern.service-now.com/service-portal?id=kb_article&n=KB0004361) every evening, and thanks to the retain policy we set in the persistent volumes, the data can be restored in a state up to 30 days prior.

## Design considerations

### Template

The Nexus application template is written as an [Ansible Playbook Bundle](https://docs.okd.io/3.11/apb_devel/writing/getting_started.html).
Nexus does not use configuration files but a database. This means complex operations to configure Nexus during provisioning.
The template internally uses the [nexus3-oss Ansible role](https://github.com/ansible-ThoTeam/nexus3-oss) for this.

### Mitigation of duplicate provisioning

This Nexus APB is not currently re-entrant:
* the `nexus3-oss` upstream role would need local persistent storage in the provisioner job to remember which Groovy scripts it already
  installed, and will fail if run again on an already configured instance without that local data.
* the `nexus3-oss` upstream role looks at `nexus_data_dir_contents` to determine whether an instance is already
  installed and take some decisions based on that. However the folder is not readily available for the provisioning APB.

This means that if a user tries to provision another
[ServiceInstance](https://docs.openshift.com/container-platform/3.11/architecture/service_catalog/index.html#service-catalog-concepts-terminology)
of Nexus in the same namespace, there will typically an error and the Kubernetes Service Catalog will
initiate [orphan mitigation](https://github.com/openservicebrokerapi/servicebroker/blob/b9bfda62c1ab195995892d61e641d0f4171a704e/spec.md#orphans).
This will result in deprovisioning of the previously existing instance.

We want to avoid this since the effort of recovering from this is non-trivial and this is really not the expected behavior
when mistakenly provisioning a duplicate instance.

While APBs would typically be re-entrant (just ensuring all resources are in the expected state),
it seems difficult to be fully re-entrant with Nexus due to the complexity of Nexus configuration, making it hard to be fail-safe.
For now, we're doing the following:
* make sure to set a label `nexus-cern-apb.cern.ch/instance-id` on each resource  with the specific ServiceInstance unique ID,
  and during deprovisioning only delete resources with the matching label. This will prevent the mitigation of the
  second provisioning that failed due to Persistent Volume checks from deleting the resources of the first instance.
* NB: this only works with an early failure in case of existing instance, since if a second provisioning starts taking place, the `nexus-cern-apb.cern.ch/instance-id`
  label on existing resources will just be overwritten with the new instance ID (and then removed by mitigation of failure in second provisioning).

### Nexus alerts when OpenShift persistent volume filling up
We have integrated a [soft quota on blob store](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-AddingaSoftQuota),
this monitors a blob store and raises an alert when it exceeds a constraint, in this case, the quota limit we set is of 200MB and will raise an alarms when
there is less than 200MB of free space in the PV. This quota limit can be manually changed by the admin of the Nexus instance (see [this](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-AddingaSoftQuota))

## Cookbook

### Invoke API

For general API access, use [cern-get-sso-cookie](http://linux.web.cern.ch/linux/docs/cernssocookie.shtml) for authentication
with a CERN account.

It is also possible to use the OpenShift CLI to bypass SSO authentication and use Nexus's local admin account.
E.g. this runs one of the scripts installed by the [nexus3-oss Ansible role](https://github.com/ansible-ThoTeam/nexus3-oss)
to delete a repo with name `central`:

```bash
# forward a local port to nexus, bypassing SSO (process runs in the background, remember to kill it when done)
oc port-forward dc/nexus 8081 &
adminpwd=$(oc get secret/nexus-admin-password -o go-template='{{index .data "nexusAdminPassword"}}' | base64 -d)
curl http://admin:${adminpwd}@localhost:8081/service/rest/v1/script/delete_repo/run -X POST -v -d '{"name": "central"}' -H 'Content-Type: text/plain'
```

### Slow Maven artifact downloads

It can take long for Maven to download all artifact from Nexus for each build.
When using GitLab CI, consider caching Maven artifacts: https://stackoverflow.com/questions/37785154/how-to-enable-maven-artifact-caching-for-gitlab-ci-runner

### Restore previous version of Nexus database
Follow [this documentation](https://help.sonatype.com/repomanager3/backup-and-restore/restore-exported-databases), note that this procedure
has to be executed by the admin of the Nexus instance.
`$data-dir` in our case is se to `/nexus-data/`

### Fix nexus data directory already in use: /nexus-data

Problem when a running nexus pod abruptly dies. In this case a replica count of 1 makes the deployment to start a new pod but it then crash-loops
because of some "lock" files. In this case we will have to remove two lock files and re-deploy the nexus instance.

Problem:
```
id: cannot find name for user ID <user_id>
Nexus data directory already in use: /nexus-data
```

Solution:
```
oc scale dc/nexus --replicas=0 -n <namespace>
oc debug dc/nexus -n <namespace>
cd /nexus-data/
# remove 2 lock files
rm lock tmp/nexus-ehcache/.lock
exit
oc scale dc/nexus --replicas=1 -n <namespace>
```


### Removing content from a S3 bucket with soft-deletion enabled
**Only for Nexus sites created before April 2020**
[Soft-deletion](https://docs.aws.amazon.com/AmazonS3/latest/dev/Versioning.html) preserves deleted objects, so special steps are required to wipe the contents of buckets where this was enabled. Follow the steps below:

Install `awscli` if needed and list the buckets:
```
yum install awscli
aws configure
# AWS Access Key ID [None]: <key here>
# AWS Secret Access Key [None]: <secret here>
# Default region name [None]:
# Default output format [None]:
aws --endpoint-url=http://s3.cern.ch s3api list-buckets
```

Execute the following command, have in mind that you will have to run the following script adding the bucket name (e.g. `sh remove_bucket_content.sh my_bucket_to_remove`):
```
#!/bin/bash

bucket=$1

set -e

echo "Removing all versions from $bucket"

versions=`aws --endpoint-url=http://s3.cern.ch s3api list-object-versions --bucket $bucket |jq '.Versions'`
markers=`aws --endpoint-url=http://s3.cern.ch s3api list-object-versions --bucket $bucket |jq '.DeleteMarkers'`

echo "removing files"
for version in $(echo "${versions}" | jq -r '.[]? | @base64'); do
    version=$(echo ${version} | base64 --decode)
    key=`echo $version | jq -r .Key`
    versionId=`echo $version | jq -r .VersionId `
    aws --endpoint-url=http://s3.cern.ch s3api delete-object --bucket $bucket --key '$key' --version-id='$versionId'
done

echo "removing delete markers"
for marker in $(echo "${markers}" | jq -r '.[]? | @base64'); do
    marker=$(echo ${marker} | base64 --decode)

    key=`echo $marker | jq -r .Key`
    versionId=`echo $marker | jq -r .VersionId `
    aws --endpoint-url=http://s3.cern.ch s3api delete-object --bucket $bucket --key '$key' --version-id='$versionId'
done
echo "Manually execution to FORCE THE DELETION of the bucket:"
echo "s3cmd --config=S3CONFIG.cfg rb s3://$bucket --force --recursive"
```

## JAVA package deploy (to) and retrieve (from) Nexus

By default in CERN context on Nexus anonymous access is disabled for download. Once you have set up your repositories you can easily get your packages via CURL or in case you want to link your Nexus repository as a source repository for dependency packages you have to configure your JAVA compiler tool (for example Maven).

Please note that on Nexus 3 hasn't got indexes on the repository. This means from your development framework you can not list the uploaded packages. You have to know by heart the version number, group id and artifact id which you want to use.

Also the current Nexus 3 API is lacking of implemented features like give me the latest package using a curl command. Etc.

#### CURL download RELEASE packages

```
>curl -u $NEXUS_REPO_USER:$NEXUS_REPO_PW -o $DESTINATION_PACKAGE_NAME "$HOST_NAME/$REPOSITORY_NAME/$PACKAGE_GROUP_ID/$ARTIFACT_ID/$ARTIFACT_VERSION/$JAVA_PACAKAGE" -L
```

* $NEXUS_REPO_USER: authentication user name (basic or LDAP) for example MyUser
* $NEXUS_REPO_PW: authentication pw (basic or LDAP) for example MyPw
* $HOST_NAME: https://localhost/repository
* $REPOSITORY_NAME: my-awesome-repository
* $PACKAGE_GROUP_ID: slash separated "my/custom/group/id"
* $DESTINATION_PACKAGE_NAME: for example my-package.jar (if you don't want to keep the version in the name)
* $JAVA_PACAKAGE: for example my-package-1.0.jar
* $ARTIFACT_ID: for example my-package
* $ARTIFACT_VERSION: for example 1.0

```
>curl -u MyUser:MyPw -o my-package.jar "https://localhost/repository/my-awesome-repository/my/custom/group/id/my-package/1.0/my-package-1.0.jar" -L
```

#### CURL download SNAPSHOT packages

Nexus stores the SNAPSHOTS differently than the releases. These are the differences:

$JAVA_PACAKAGE: for example my-package-1.0-20190321.164448-1.jar (there is a timestamp at the end !!!)
$ARTIFACT_VERSION: for example 1.0-SNAPSHOT

```
>curl -u MyUser:MyPw -o my-package.jar "https://localhost/repository/my-awesome-repository/my/custom/group/id/my-package/1.0-SNAPSHOT/my-package-1.0-20190321.164448-1.jar" -L
```

#### Maven configuration

Maven build comes with a lots of good features and plugins. It gives a good standardized way to build your JAVA packages. With Nexus 3 we advise you to use the default maven deploy plugin.
With that the maven snapshot and release package deployment can be easily separated after compilation. The compiler will make the separation itself based on the version number, in case the version number ends with "-SNAPSHOT" that will be a snapshot release (In case you follow the [Maven version numbering](https://docs.oracle.com/middleware/1212/core/MAVEN/maven_version.htm#MAVEN8855)).
For authentication you can separate the credentials into a config file. This gives you the flexibility to separation sensitive information from the package itself.

Sample configuration files (pom.xml and settings.xml) can be founded under the maven-setting-templates folder.
I advise you to use the snapshot profile for local development and leave the final package build for the CI/CD.

**Maven gives the following nice ways for repositories**:

* create a global maven setting file if you want to do a maven (in CentOs usually located under /home/$YourUser/.m2/settings.xml)
This is good for developers, in case you specify there the settings. You can execute the build from your framework and you don't have to link manually the settings via command line argument. Please note that you have to enable the right profile (snapshot or release like the "my-profile" in the activeProfiles)!

* command line argument which contains the custom maven settings
With -s you can link a settings.xml file and with -P you can enable the right profile (snapshot or release). This approach is good for CI/CD pipelines.

```
>mvn clean compile test package deploy -s ${MAVEN_SETTINGS_LOCATION} -P snapshot-build-profile
```

## How to update Nexus

In [a feature branch](https://docs.gitlab.com/ee/workflow/workflow.html):

1. Update Nexus version: update variable `NEXUS_IMAGE` in `.gitlab-ci.yml` to the latest version
  (e.g. `NEXUS_IMAGE: sonatype/nexus3:3.21.2`).
2. Update the upstream Ansible role. Find the latest stable git tag for the nexus3-oss role
  in https://github.com/ansible-ThoTeam/nexus3-oss/tags and update the git submodule:
  ```bash
  cd roles/nexus3-oss
  git submodule update --init
  git checkout <tag>
  git submodule deinit .
  ```
3. Update the customizations of the upstream role. We cannot use the upstream role as-is as it installs Nexus,
  but we use the upstream Docker image so Nexus is already install in our case. We only want to _configure_ it
  with the Nexus role:
    1. Copy `roles/nexus3-oss/tasks/main.yml` into our customized version `roles/nexus3-oss-customizations/tasks/custom_main.yml`
    2. Comment everything up to and including the call to `nexus_install.yml` task.
    3. At the top of `custom_main.yml`, insert (copy/paste) the set of tasks from `roles/nexus3-oss/tasks/nexus_install.yml`
       that register the groovy scripts, since we need them for the configuration tasks. (search for `groovy` in `nexus_install.yml`)
    4. Then at the end of the `custom_main.yml`, insert the `Configure timeouts` task from the previous `custom_main.yml`.

### Testing the new version

1. We have set in `gitlab-ci` as provisioning test with `oc cluster up` of a Nexus instance in every push.
   Once you are happy with the changes and the tests in `gitlab-ci` succeed, deploy the image in `openshift-dev`.
2. Upgrade an existing instance: using an existing test instance (e.g. `test-repomgr3`), manually update
   the DeploymentConfig to disable the `ImageChange` trigger and set the image to the new version. Verify that
   upgrade is successful.
3. Deploy a new instance with the new provisioning code and new Nexus version in a testing instance of `openshift-dev`.
   This can be done in a debug mode of the `gitlab-ci` test, triggering a new pipeline with the key `CI_INTERACTIVE_DEBUG`

#### Testing your changes a bit deeper

We can do this with `oc cluster up`

```
host_ip=$(docker run --network=host --rm alpine ip -family inet -oneline addr show dev eth0 | awk '{print $4}' | cut -d / -f 1)
oc cluster up --public-hostname=${host_ip} --enable=*,service-catalog,template-service-broker,automation-service-broker,persistent-volumes
oc login -u system:admin
```
      2. Import cern-sso-proxy template:
```
oc create -n openshift -f https://gitlab.cern.ch/paas-tools/cern-sso-proxy/raw/master/templates/cern-sso-proxy.yaml
```

      3. Clone the [code](https://gitlab.cern.ch/vcs/nexus-cern-apb.git) and do your changes. Once you want to test then, modify the code as following:
Comment the following code from `nexus-cern-apb/roles/provision-nexus-cern-apb/tasks/main.yml`
```
#- type: ImageChange
#    imageChangeParams:
#    automatic: true
#    containerNames:
#    - nexus3
#       from:
#       kind: ImageStreamTag
#       name: nexus-cern:stable
#       namespace: openshift
```
and point `image: " "` to the desired [upstream release](https://hub.docker.com/r/sonatype/nexus3/tags)
Now you can build the image and push it to your registry

      4. Run the following job changing the image pointing to the image just created
```
 apiVersion: batch/v1
 kind: Job
 metadata:
   name: nexus-install
 spec:
   activeDeadlineSeconds: 9000000
   backoffLimit: 5
   template:
     spec:
       containers:
       - args:
         - provision
         - --extra-vars
         - '{"apb_nexus_admin_egroup":"it-service-vcs", "apb_desired_storage_size": "10Gi","namespace":"test-isp-nexus"}'
         image: <custom_image>
         imagePullPolicy: Always
         name: nexus-install
         env:
         - name: POD_NAME
           valueFrom:
             fieldRef:
               apiVersion: v1
               fieldPath: metadata.name
         - name: POD_NAMESPACE
           valueFrom:
             fieldRef:
               apiVersion: v1
               fieldPath: metadata.namespace
         - name: ANSIBLE_DEBUG
           value: "True"
         - name: ANSIBLE_STDOUT_CALLBACK
           value: "debug"
         - name: ANSIBLE_DISPLAY_ARGS_TO_STDOUT
           value: "True"
       restartPolicy: Never
       serviceAccount: nexus-install
```
      4. If anything fails and you need to debug it, you have to do the following:
```
oc port-forward <nexus_pod> <nexus_port>
curl -X POST -u "admin:admin_password" --header 'Content-Type: text/plain' http://localhost:8081/service/rest/v1/script/create_blobstores_from_list/run -d @json_object -v
```
Now go to `settings > Support > Logging > <desired_type>` and select `trace`, after you will have all the trace in the `Log Viewer` area
Example of a json_object from ansible to create a bucket
```
[{ "name": "s3-blobstore","type": "S3", "config": { "bucket":"nexus-cern-test", "endpoint": "https://s3.cern.ch", "region": "us-east-1", "expiration": "-1", "accessKeyId": "<acces_key>", "secretAccessKey": "<secre_key>"}}]
```

### Deploying the new version

After reviewing the changes in a Merge Request, merge to master and when happy, run the pipelines to trigger the image deployment
in `openshift-dev`.
Once all the instances were correctly upgraded and you are able to create a new Nexus instance, open
a [SSB](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003166) to announce the Nexus upgrade
in `openshift prod`. During the day of the intervention, run the pipeline to deploy the changes in prod.

### What to do in case changes are necessary in backup script
**Only for Nexus sites created before April 2020**
During the upgrade to 3.19, some changes in the Nexus object model made it necessary to
[modify the backup script](https://gitlab.cern.ch/vcs/nexus-cern-apb/commit/ea17c146a41f49f555cc93c8ffbdc50c5a0d5c7e),
which now failed.

But the script needs to be updated on all existing instances.

To find namespaces that contain Nexus instances:

```bash
oc get serviceinstance --all-namespaces | grep nexus | awk '{print $1}'
```

To update an instance (set `namespace` as appropriate):

```bash
namespace="test-repomgr3"
notificationEmail="$(oc get secret -n $namespace $(oc get serviceinstance  -n $namespace -o json | jq -r '.items[] | .spec.parametersFrom[0].secretKeyRef.name') -o json | jq -r .data.parameters | base64 -d | jq -r .apb_nexus_admin_egroup)@cern.ch"
script=$(cat <<'EOF'
export HOME=/tmp
export PATH=$PATH:/tmp/.local/bin
pip install --user j2cli # goes to $HOME/.local/bin
curl -sL https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 > /tmp/.local/bin/jq
chmod +x /tmp/.local/bin/jq
git clone https://gitlab.cern.ch/vcs/nexus-cern-apb.git /tmp/nexus-cern-apb
cd /tmp/nexus-cern-apb
git checkout updateNexus3.19test
echo '{"nexus_cern_s3_blobstore_name": "s3-blobstore", "nexus_backup_dir": "/tmp", "nexus_cern_backup_s3_prefix": "nexus-db-backup"}' > j2values.json
backupScript=$(j2 roles/provision-nexus-cern-apb/templates/backup-db-to-s3.groovy.j2 j2values.json)
jq -n --arg content "$backupScript" --arg notificationEmail "${notificationEmail}" '{"name": "nexus-cern-export-database-to-s3", "typeId": "script", "cron":"0 30 21 * * ?", "task_alert_email": $notificationEmail, "taskProperties": { "language": "groovy", "source": $content }}' > createTaskArgs.json
curl -X POST -u "admin:${nexusAdminPassword}" --header 'Content-Type: text/plain' http://nexus:8081/service/rest/v1/script/create_task/run -d @createTaskArgs.json
EOF
)
# NB: if running as non-cluster-admin user, remove the securityContext to run as default unprivileged user
overrides=$(jq -n --arg notificationEmail "${notificationEmail}" --arg script "${script}" '
{
"apiVersion": "v1",
"spec": {
    "containers": [
        {
            "name": "update-backup-task",
            "image": "python",
            "command": ["bash", "-c", $script],
            "envFrom": [{"secretRef": {"name": "nexus-admin-password"}}],
            "env": [{"name": "notificationEmail", "value": $notificationEmail}],
            "tty": true,
            "stdin": true,
            "securityContext": {
                "runAsUser": 1001
            }

        }
    ]
  }
}
')
oc run -n $namespace -i -t --rm update-backup-task --restart=Never --image='python' --overrides="${overrides}"
```
